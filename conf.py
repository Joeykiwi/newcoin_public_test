import os
import socket

def get_config_info() :

    user_input = input("Please enter RPCUSER : ")
    rpc_user = user_input    
    password_input = input("Please enter RPCPASSWORD : ")
    rpc_password = password_input
    privkey_input = input("Please enter masternode private key : ")
    private_key = privkey_input

    return rpc_user, rpc_password, private_key
    


if __name__ == '__main__':
    user, password, privkey = get_config_info()

    if (user == password) :
        while user==password :
            print ("\033[1;31m RPCUSER and RPCPASSWORD must not be same!! \033[0;0m")
            user, password, privkey = get_config_info()

    

    with open("./newcoin.conf", 'a') as conf :
        conf.write("rpcuser=" + user + '\n')
        conf.write("rpcpassword=" + password + '\n')
        conf.write("daemon=1\n")
        conf.write("server=1\n")
        conf.write("listen=1\n")
        conf.write("port=50007\n")
        conf.write("masternode=1\n")
        conf.write("masternodeaddr=127.0.0.1:8888\n")
        conf.write("masternodeprivkey=" + privkey + "\n")