// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "assert.h"

#include "chainparams.h"
#include "main.h"
#include "util.h"

#include <boost/assign/list_of.hpp>

using namespace boost::assign;

struct SeedSpec6 {
    uint8_t addr[16];
    uint16_t port;
};

#include "chainparamsseeds.h"

//
// Main network
//

// Convert the pnSeeds array into usable address objects.
static void convertSeeds(std::vector<CAddress> &vSeedsOut, const unsigned int *data, unsigned int count, int port)
{
     // It'll only connect to one or two seed nodes because once it connects,
     // it'll get a pile of addresses with newer timestamps.
     // Seed nodes are given a random 'last seen time' of between one and two
     // weeks ago.
     const int64_t nOneWeek = 7*24*60*60;
    for (unsigned int k = 0; k < count; ++k)
    {
        struct in_addr ip;
        unsigned int i = data[k], t;
        
        // -- convert to big endian
        t =   (i & 0x000000ff) << 24u
            | (i & 0x0000ff00) << 8u
            | (i & 0x00ff0000) >> 8u
            | (i & 0xff000000) >> 24u;
        
        memcpy(&ip, &t, sizeof(ip));
        
        CAddress addr(CService(ip, port));
        addr.nTime = GetTime()-GetRand(nOneWeek)-nOneWeek;
        vSeedsOut.push_back(addr);
    }
}

class CMainParams : public CChainParams {
public:
    CMainParams() {
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0xce;
        pchMessageStart[1] = 0x6d;
        pchMessageStart[2] = 0x4c;
        pchMessageStart[3] = 0x63;
        vAlertPubKey = ParseHex("04ac1f0b7d2bc73c5eb8f057fd2ab32b6d2f14b270a87e2ba7ca622a6f362ef951cf3056369d70c807bc65346e0ab37b0ac16310400a586dd3dfc8c6dd6d949d55");
        nDefaultPort = 50007;
        nRPCPort = 50008;
        bnProofOfWorkLimit = CBigNum(~uint256(0) >> 16);
        uint256 bnProofOfWorkLimit_cop = ~uint256(0) >> 16;

        // Build the genesis block. Note that the output of the genesis coinbase cannot
        // be spent as it did not originally exist in the database.
        //
        //CBlock(hash=000001faef25dec4fbcf906e6242621df2c183bf232f263d0ba5b101911e4563, ver=1, hashPrevBlock=0000000000000000000000000000000000000000000000000000000000000000, hashMerkleRoot=12630d16a97f24b287c8c2594dda5fb98c9e6c70fc61d44191931ea2aa08dc90, nTime=1393221600, nBits=1e0fffff, nNonce=164482, vtx=1, vchBlockSig=)
        //  Coinbase(hash=12630d16a9, nTime=1393221600, ver=1, vin.size=1, vout.size=1, nLockTime=0)
        //    CTxIn(COutPoint(0000000000, 4294967295), coinbase 00012a24323020466562203230313420426974636f696e2041544d7320636f6d6520746f20555341)
        //    CTxOut(empty)
        //  vMerkleTree: 12630d16a9
        const char* pszTimestamp = "26 Jan 2018 Newcoin Rising Today.";
        std::vector<CTxIn> vin;
        vin.resize(1);
        vin[0].scriptSig = CScript() << 0 << CBigNum(42) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
        std::vector<CTxOut> vout;
        vout.resize(1);
        vout[0].SetEmpty();
        CTransaction txNew(1, 1515548673, vin, vout, 0);
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock = 0;
        genesis.hashMerkleRoot = genesis.BuildMerkleTree();
        genesis.nVersion = 1;
        genesis.nTime    = 1515548673;
        genesis.nBits    = 520159231; 
        genesis.nNonce   = 46243;

        // hashGenesisBlock = uint256("0x0000a24f8bf3dac7029ecff5e054bd8abf0140728112eaa095d8ce78495310c3");
        // if (true && genesis.GetHash() != hashGenesisBlock)
        // {
        //     printf("recalculating params for mainnet.\n");
        //     printf("old mainnet genesis nonce: %u\n", genesis.nNonce);
        //     printf("old mainnet genesis hash:  %s\n", hashGenesisBlock.ToString().c_str());
        //     // deliberately empty for loop finds nonce value.
        //     for(genesis.nNonce = 0; genesis.GetHash() > bnProofOfWorkLimit_cop; genesis.nNonce++){ } 
        //     printf("new mainnet genesis merkle root: %s\n", genesis.hashMerkleRoot.ToString().c_str());
        //     printf("new mainnet genesis nonce: %u\n", genesis.nNonce);
        //     printf("new mainnet genesis hash: %s\n", genesis.GetHash().ToString().c_str());
        // }


        hashGenesisBlock = genesis.GetHash();

        assert(hashGenesisBlock == uint256("0x0000a24f8bf3dac7029ecff5e054bd8abf0140728112eaa095d8ce78495310c3"));
        assert(genesis.hashMerkleRoot == uint256("0x22a61bc6be4be5205b4449b3ccc4178cf2d5b6662ee5a52720065bb24a4cbbd0"));

        
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,66);
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,85);
        base58Prefixes[SECRET_KEY] =     std::vector<unsigned char>(1,153);
        base58Prefixes[STEALTH_ADDRESS] = std::vector<unsigned char>(1,40);
        base58Prefixes[EXT_PUBLIC_KEY] = list_of(0x04)(0x78)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
        base58Prefixes[EXT_SECRET_KEY] = list_of(0x04)(0x78)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();

        //we put something here
        // vFixedSeeds.clear();
        // vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("0",  "13.124.246.57"));
        vSeeds.push_back(CDNSSeedData("1",  "52.78.63.246"));
        convertSeeds(vFixedSeeds, pnSeed, ARRAYLEN(pnSeed), nDefaultPort);

        nPoolMaxTransactions = 3;
        //strSporkKey = "046f78dcf911fbd61910136f7f0f8d90578f68d0b3ac973b5040fb7afb501b5939f39b108b0569dca71488f5bbf498d92e4d1194f6f941307ffd95f75e76869f0e";
        //strMasternodePaymentsPubKey = "046f78dcf911fbd61910136f7f0f8d90578f68d0b3ac973b5040fb7afb501b5939f39b108b0569dca71488f5bbf498d92e4d1194f6f941307ffd95f75e76869f0e";
        strDarksendPoolDummyAddress = "T7FBJNGDmEsU5wx2m3xw85N8kRgCqA8S7L";
        nLastPOWBlock = 2000;
        nPOSStartBlock = 201;
    }

    virtual const CBlock& GenesisBlock() const { return genesis; }
    virtual Network NetworkID() const { return CChainParams::MAIN; }

    virtual const vector<CAddress>& FixedSeeds() const {
        return vFixedSeeds;
    }
protected:
    CBlock genesis;
    vector<CAddress> vFixedSeeds;
};
static CMainParams mainParams;


//
// Testnet
//

class CTestNetParams : public CMainParams {
public:
    CTestNetParams() {
        // The message start string is designed to be unlikely to occur in normal data.
        // The characters are rarely used upper ASCII, not valid as UTF-8, and produce
        // a large 4-byte int at any alignment.
        pchMessageStart[0] = 0xce;
        pchMessageStart[1] = 0x6d;
        pchMessageStart[2] = 0x4c;
        pchMessageStart[3] = 0x63;
        bnProofOfWorkLimit = CBigNum(~uint256(0) >> 16);
        uint256 bnProofOfWorkLimit_cop = ~uint256(0) >> 16;
        vAlertPubKey = ParseHex("0475ffccdc08cf600aa4747a207bae61fb1d8c9bc5253878742a39eecda8fcb87b78836423f52ae20bc92263cb0408b24f523c89c1458f739ca5094d");
        nDefaultPort = 20114;
        nRPCPort = 20115;
        strDataDir = "testnet";

        // Modify the testnet genesis block so the timestamp is valid for a later start.
        genesis.nBits  = 520159231; 
        genesis.nNonce = 46243;

        // hashGenesisBlock = uint256("0x01");
        // if (true && genesis.GetHash() != hashGenesisBlock)
        // {
        //     printf("recalculating params for mainnet.\n");
        //     printf("old mainnet genesis nonce: %u\n", genesis.nNonce);
        //     printf("old mainnet genesis hash:  %s\n", hashGenesisBlock.ToString().c_str());
        //     // deliberately empty for loop finds nonce value.
        //     for(genesis.nNonce = 0; genesis.GetHash() > bnProofOfWorkLimit_cop; genesis.nNonce++){ } 
        //     printf("new mainnet genesis merkle root: %s\n", genesis.hashMerkleRoot.ToString().c_str());
        //     printf("new mainnet genesis nonce: %u\n", genesis.nNonce);
        //     printf("new mainnet genesis hash: %s\n", genesis.GetHash().ToString().c_str());
        // }
        hashGenesisBlock = genesis.GetHash();
        
        assert(hashGenesisBlock == uint256("0x0000a24f8bf3dac7029ecff5e054bd8abf0140728112eaa095d8ce78495310c3"));

        vFixedSeeds.clear();
        vSeeds.clear();

        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,127);
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,196);
        base58Prefixes[SECRET_KEY]     = std::vector<unsigned char>(1,239);
        base58Prefixes[STEALTH_ADDRESS] = std::vector<unsigned char>(1,40);
        base58Prefixes[EXT_PUBLIC_KEY] = list_of(0x04)(0x35)(0x87)(0xCF).convert_to_container<std::vector<unsigned char> >();
        base58Prefixes[EXT_SECRET_KEY] = list_of(0x04)(0x35)(0x83)(0x94).convert_to_container<std::vector<unsigned char> >();

        convertSeeds(vFixedSeeds, pnTestnetSeed, ARRAYLEN(pnTestnetSeed), nDefaultPort);

        nLastPOWBlock = 0x7fffffff;
    }
    virtual Network NetworkID() const { return CChainParams::TESTNET; }
};
static CTestNetParams testNetParams;


static CChainParams *pCurrentParams = &mainParams;

const CChainParams &Params() {
    return *pCurrentParams;
}

void SelectParams(CChainParams::Network network) {
    switch (network) {
        case CChainParams::MAIN:
            pCurrentParams = &mainParams;
            break;
        case CChainParams::TESTNET:
            pCurrentParams = &testNetParams;
            break;
        default:
            assert(false && "Unimplemented network");
            return;
    }
}

bool SelectParamsFromCommandLine() {
    
    bool fTestNet = GetBoolArg("-testnet", false);
    
    if (fTestNet) {
        SelectParams(CChainParams::TESTNET);
    } else {
        SelectParams(CChainParams::MAIN);
    }
    return true;
}
